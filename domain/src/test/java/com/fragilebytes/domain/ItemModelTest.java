/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fragilebytes.domain;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * class description: this class asserts model constructors
 * issues:
 */

public class ItemModelTest {

  private static final int FAKE_ID = 8;
  private static final String FAKE_VALUE_ONE = "value_one";
  private static final String FAKE_VALUE_TWO = "value_two";

  private ItemModel item;

  @Test
  public void assert_itemModel_fullConstructor() throws Exception{
    item = new ItemModel(FAKE_ID, FAKE_VALUE_ONE, FAKE_VALUE_TWO);
    final int itemId = item.getId();
    final String valueOne = item.getValueOne();
    final String valueTwo = item.getValueTwo();

    assertThat(itemId, is(FAKE_ID));
    assertThat(valueOne, is(FAKE_VALUE_ONE));
    assertThat(valueTwo, is(FAKE_VALUE_TWO));
  }

  @Test
  public void assert_itemModel_simplifiedConstructor() throws Exception{
    item = new ItemModel(FAKE_VALUE_ONE, FAKE_VALUE_TWO);
    final int itemId = item.getId();
    final String valueOne = item.getValueOne();
    final String valueTwo = item.getValueTwo();

    assertThat(itemId, is(0));
    assertThat(valueOne, is(FAKE_VALUE_ONE));
    assertThat(valueTwo, is(FAKE_VALUE_TWO));
  }
}
