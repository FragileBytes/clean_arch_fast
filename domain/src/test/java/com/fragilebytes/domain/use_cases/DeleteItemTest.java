package com.fragilebytes.domain.use_cases;

import com.fragilebytes.domain.ItemModel;
import com.fragilebytes.domain.executor.PostExecutionThread;
import com.fragilebytes.domain.executor.ThreadExecutor;
import com.fragilebytes.domain.repository.UserRepository;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class DeleteItemTest {

  private static final int ITEM_ID = 123;
  private static final String ITEM_VALUE_ONE = "value one";
  private static final String ITEM_VALUE_TWO = "value two";

  private DeleteItem deleteItem;

  @Mock
  private UserRepository mockUserRepository;
  @Mock
  private ThreadExecutor mockThreadExecutor;
  @Mock
  private PostExecutionThread mockPostExecutionThread;

  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  @Before
  public void setUp() {
    deleteItem = new DeleteItem(mockUserRepository, mockThreadExecutor,
        mockPostExecutionThread);
  }

  @Test
  public void assert_DeleteItemUseCaseObservable_POSETIVE() {
    ItemModel item = new ItemModel(ITEM_ID, ITEM_VALUE_ONE, ITEM_VALUE_TWO);
    deleteItem.buildUseCaseObservable(item);

    verify(mockUserRepository).deleteItem(item);
    verifyNoMoreInteractions(mockUserRepository);
    verifyZeroInteractions(mockPostExecutionThread);
    verifyZeroInteractions(mockThreadExecutor);
  }

  @Test
  public void assert_ShouldFailWhenNoOrEmptyParameters() {
    expectedException.expect(NullPointerException.class);
    deleteItem.buildUseCaseObservable(null);
  }
}
