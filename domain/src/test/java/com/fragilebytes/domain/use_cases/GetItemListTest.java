package com.fragilebytes.domain.use_cases;

import com.fragilebytes.domain.executor.PostExecutionThread;
import com.fragilebytes.domain.executor.ThreadExecutor;
import com.fragilebytes.domain.repository.UserRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class GetItemListTest {

  private GetItemList getItemList;

  @Mock
  private ThreadExecutor mockThreadExecutor;
  @Mock
  private PostExecutionThread mockPostExecutionThread;
  @Mock
  private UserRepository mockUserRepository;

  @Before
  public void setUp() {
    getItemList = new GetItemList(mockUserRepository, mockThreadExecutor,
        mockPostExecutionThread);
  }

  @Test
  public void assert_GetItemListUseCaseObservable_POSETIVE() {
    getItemList.buildUseCaseObservable(null);

    verify(mockUserRepository).getItemList();
    verifyNoMoreInteractions(mockUserRepository);
    verifyZeroInteractions(mockThreadExecutor);
    verifyZeroInteractions(mockPostExecutionThread);
  }
}
