package com.fragilebytes.domain.use_cases;

import com.fragilebytes.domain.ItemModel;
import com.fragilebytes.domain.executor.PostExecutionThread;
import com.fragilebytes.domain.executor.ThreadExecutor;
import com.fragilebytes.domain.repository.UserRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * This class is an implementation of {@link UseCase} that represents a use case for
 * retrieving a collection of all {@link ItemModel}.
 */
public class UpdateItem extends UseCase<ItemModel, ItemModel> {

  private final UserRepository userRepository;

  @Inject
  UpdateItem(UserRepository userRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
    super(threadExecutor, postExecutionThread);
    this.userRepository = userRepository;
  }

  @Override
  public Observable<ItemModel> buildUseCaseObservable(ItemModel item) throws NullPointerException{
    if (item == null) throw new NullPointerException("getItem to update equal to Null");
    return this.userRepository.updateItem(item);
  }
}
