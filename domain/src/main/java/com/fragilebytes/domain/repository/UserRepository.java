package com.fragilebytes.domain.repository;

import com.fragilebytes.domain.ItemModel;

import java.util.List;

import io.reactivex.Observable;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * Interface that represents a Repository for getting {@link ItemModel} related data.
 */
public interface UserRepository {
  /**
   * Get an {@link Observable} which will emit a List of {@link ItemModel}.
   */
  Observable<List<ItemModel>> getItemList();

  /**
   * Get an {@link Observable} which will emit a {@link ItemModel}.
   *
   * @param itemId The getItem id used to retrieve getItem data.
   */
  Observable<ItemModel> getItem(final int itemId);

  Observable<ItemModel> updateItem(ItemModel item);
  Observable<ItemModel> addItem(ItemModel item);
  Observable<ItemModel> deleteItem(ItemModel item);

}
