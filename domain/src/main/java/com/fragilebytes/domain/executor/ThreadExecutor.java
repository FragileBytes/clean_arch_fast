package com.fragilebytes.domain.executor;

import java.util.concurrent.Executor;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * Executor implementation can be based on different frameworks or techniques of asynchronous
 * execution, but every implementation will execute the
 * {@link .UseCase} out of the UI thread.
 */
public interface ThreadExecutor extends Executor {}
