package com.fragilebytes.domain;

import io.reactivex.annotations.NonNull;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 11/06/17, modified: 11/06/17
 * class description: ItemModel shared across packages
 * issues:
 */

public class ItemModel {
    private int mId;
    @NonNull
    private String mValueOne;
    @NonNull
    private String mValueTwo;

    public ItemModel(int id, String valueOne, String valueTwo){
        mId = id;
        mValueOne = valueOne;
        mValueTwo = valueTwo;
    }

    public ItemModel(String valueOne, String valueTwo){
        mValueOne = valueOne;
        mValueTwo = valueTwo;
    }


    public String getValueOne() {
        return mValueOne;
    }

    public void setValueOne(String valueOne) {
        mValueOne = valueOne;
    }

    public String getValueTwo() {
        return mValueTwo;
    }

    public void setValueTwo(String valueTwo) {
        mValueTwo = valueTwo;
    }

    public int getId(){
        return mId;
    }

    public void setId(int id){
     mId = id;
    }
}