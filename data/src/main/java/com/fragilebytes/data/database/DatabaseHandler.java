package com.fragilebytes.data.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.fragilebytes.data.R;
import com.fragilebytes.domain.ItemModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * class description: this is a helper class that will handle SQLite requests
 * issues:
 */
@Singleton
public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "item_db";

    // Contacts table name
    private static final String TABLE_ITEMS = "items";

    // Items Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_VALUE_ONE = "value_one";
    private static final String KEY_VALUE_TWO = "value_two";

    private Context mContext;

    @Inject
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_ITEMS + "("
                + KEY_ID + " INTEGER PRIMARY KEY, "
                + KEY_VALUE_ONE + " TEXT, "
                + KEY_VALUE_TWO + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEMS);

        // Create tables again
        onCreate(db);
    }


    // Adding new getItem
    public ItemModel addItem(ItemModel item) throws Exception {

        item = new ItemModel(mContext.getString(R.string.default_value_one), mContext.getString(R.string.default_value_two));
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_VALUE_ONE, item.getValueOne());
        values.put(KEY_VALUE_TWO, item.getValueTwo());

        // Inserting Row
        item.setId((int) db.insert(TABLE_ITEMS, null, values));
        db.close(); // Closing database connection
        return item;
    }

    // Getting All getItemList
    public List<ItemModel> getAllItems() {
        List<ItemModel> itemList = new ArrayList<ItemModel>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_ITEMS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ItemModel item = new ItemModel(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
                // Adding getItem to list
                itemList.add(item);
            } while (cursor.moveToNext());
        }
        //returning getItem list
        return itemList;
    }

    // Updating single getItem
    public int updateItem(ItemModel item) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_VALUE_ONE, item.getValueOne());
        values.put(KEY_VALUE_TWO, item.getValueTwo());

        // updating row
        return db.update(TABLE_ITEMS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(item.getId()) });
    }

    //deleting single getItem
    public void deleteItem(ItemModel item) throws Exception {

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ITEMS, KEY_ID + " = ?",
                new String[] {String.valueOf(item.getId())});
        db.close();
    }

    //get single getItem by id
    public ItemModel getItem(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_ITEMS, new String[] { KEY_ID,
                        KEY_VALUE_ONE, KEY_VALUE_TWO }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        ItemModel item = new ItemModel(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
        // return getItem
        return item;
    }
}
