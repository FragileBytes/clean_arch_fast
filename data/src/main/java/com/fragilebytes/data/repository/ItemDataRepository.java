package com.fragilebytes.data.repository;

import com.fragilebytes.data.repository.datasource.ItemModelStoreFactory;
import com.fragilebytes.domain.ItemModel;
import com.fragilebytes.domain.repository.UserRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 *
 * */
@Singleton
public class ItemDataRepository implements UserRepository {
  private ItemModelStoreFactory mItemStoreFactory;
  @Inject
  ItemDataRepository(ItemModelStoreFactory itemModelStoreFactory){
    mItemStoreFactory = itemModelStoreFactory;
  }

  //retrieves all items
  @Override
  public Observable<List<ItemModel>> getItemList() {
    return mItemStoreFactory.create().getAll();
  }

  //get one item by id
  @Override
  public Observable<ItemModel> getItem(int userId) {
    return mItemStoreFactory.create().get(userId);
  }

  //update item
  @Override
  public Observable<ItemModel> updateItem(ItemModel item) {
    return mItemStoreFactory.create().update(item);
  }

  //add item
  @Override
  public Observable<ItemModel> addItem(ItemModel item) {
    return mItemStoreFactory.create().add(item);
  }

  //delete item
  @Override
  public Observable<ItemModel> deleteItem(ItemModel item) {
    return mItemStoreFactory.create().delete(item);
  }
}
