package com.fragilebytes.data.repository.datasource;

import android.content.Context;

import com.fragilebytes.data.database.DatabaseHandler;
import com.fragilebytes.domain.ItemModel;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * class description:
 * issues:
 */
public class LocalItemModelStore implements ItemModelStore {
    private DatabaseHandler mDatabaseHandler;

    LocalItemModelStore(Context context){
        mDatabaseHandler = new DatabaseHandler(context);
    }

    public Observable<ItemModel> get(final int itemId) {
        return Observable.create(new ObservableOnSubscribe<ItemModel>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<ItemModel> emitter) throws Exception {
                ItemModel item = mDatabaseHandler.getItem(itemId);

                if (item != null) {
                    emitter.onNext(item);
                    emitter.onComplete();
                } else {
                    emitter.onError(new Throwable("Item not found"));
                }
            }
        });
    }

    public Observable<List<ItemModel>> getAll() {
        return Observable.create(new ObservableOnSubscribe<List<ItemModel>>() {

            @Override
            public void subscribe(@NonNull ObservableEmitter<List<ItemModel>> emitter) throws Exception {

                List<ItemModel> list = mDatabaseHandler.getAllItems();

                emitter.onNext(list);
                emitter.onComplete();
            }
        });
    }

    public Observable<ItemModel> add(final ItemModel item) {
        return Observable.create(new ObservableOnSubscribe<ItemModel>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<ItemModel> emitter) throws Exception {
                ItemModel newItem = mDatabaseHandler.addItem(item);
                emitter.onNext(newItem);
                emitter.onComplete();
            }
        });
    }

    public Observable<ItemModel> delete(final ItemModel item) {
        return Observable.create(new ObservableOnSubscribe<ItemModel>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<ItemModel> emitter) throws Exception {
                mDatabaseHandler.deleteItem(item);
                emitter.onNext(item);
                emitter.onComplete();
            }
        });
    }

    public Observable<ItemModel> update(final ItemModel item) {
        return Observable.create(new ObservableOnSubscribe<ItemModel>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<ItemModel> emitter) throws Exception {
                mDatabaseHandler.updateItem(item);
                emitter.onNext(item);
                emitter.onComplete();
            }
        });
    }
}
