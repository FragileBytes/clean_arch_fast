package com.fragilebytes.data.repository.datasource;

import com.fragilebytes.domain.ItemModel;

import java.util.List;

import io.reactivex.Observable;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * class description:
 * issues:
 */
public interface ItemModelStore {
    Observable<ItemModel> get(final int itemId);
    Observable<List<ItemModel>> getAll();
    Observable<ItemModel> add(final ItemModel item);
    Observable<ItemModel> delete(final ItemModel item);
    Observable<ItemModel> update(final ItemModel item);
}
