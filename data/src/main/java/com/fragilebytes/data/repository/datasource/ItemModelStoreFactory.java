package com.fragilebytes.data.repository.datasource;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Inject;
import javax.inject.Singleton;
/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * this class have to handle how ad from where data shall be retrieved
 * */
@Singleton
public class ItemModelStoreFactory {

  private final Context context;
  @Inject
  ItemModelStoreFactory(@NonNull Context context) {
    this.context = context.getApplicationContext();
  }
  public ItemModelStore create() {
    ItemModelStore userDataStore;
    //here to decide local store or web
    //we have only item on a local store
    userDataStore = new LocalItemModelStore(context);

    return userDataStore;
  }
}
