package com.fragilebytes.data.repository.datasource;

import android.content.Context;

import com.fragilebytes.data.database.DatabaseHandler;
import com.fragilebytes.domain.ItemModel;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * class description:
 * issues:
 */
@RunWith(MockitoJUnitRunner.class)
public class LocalItemModelStoreTest {

    private static final int MOCK_ID = 11;
    private static final String MOCK_VALUE_ONE = "value one";
    private static final String MOCK_VALUE_TWO = "value two";

    private LocalItemModelStore modelStore;
    private ItemModel item;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    Context mockContext;

    @Mock
    DatabaseHandler mockDatabaseHandler;

    @Before
    public void setUp() {
        modelStore = new LocalItemModelStore(mockContext);
        item = new ItemModel(MOCK_ID, MOCK_VALUE_ONE, MOCK_VALUE_TWO);
    }

    @Test
    public void assert_GetItemModelList() {


    }
}
