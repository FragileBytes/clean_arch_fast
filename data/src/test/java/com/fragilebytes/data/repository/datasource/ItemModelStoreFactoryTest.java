package com.fragilebytes.data.repository.datasource;

import com.fragilebytes.data.ApplicationTestBase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.robolectric.RuntimeEnvironment;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * class description:
 * issues:
 */
public class ItemModelStoreFactoryTest extends ApplicationTestBase {
    private ItemModelStoreFactory storeFactory;
    @Mock
    LocalItemModelStore mockLocalItemStore;

    @Before
    public void setUp() {
        storeFactory = new ItemModelStoreFactory(RuntimeEnvironment.application);
    }

    @Test
    public void assert_CreateLocalStore() {
        ItemModelStore itemStore = storeFactory.create();
        assertThat(itemStore, is(notNullValue()));
        assertThat(itemStore, is(instanceOf(LocalItemModelStore.class)));
    }
}