package com.fragilebytes.data.repository;

import com.fragilebytes.data.repository.datasource.ItemModelStore;
import com.fragilebytes.data.repository.datasource.ItemModelStoreFactory;
import com.fragilebytes.domain.ItemModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ItemDataRepositoryTest {

  private static final int MOCK_ID = 123;
  private static final String MOCK_VALUE_ONE = "value one";
  private static final String MOCK_VALUE_TWO = "value two";

  private ItemDataRepository itemDataRepository;

  @Mock
  private ItemModelStoreFactory mockStoreFactory;
  @Mock
  private ItemModelStore mockModelStore;
  @Mock
  private ItemModel mockItem;

  private ItemModel item;

  @Before
  public void setUp() {
    itemDataRepository = new ItemDataRepository(mockStoreFactory);
    given(mockStoreFactory.create()).willReturn(mockModelStore);
    item = new ItemModel(MOCK_ID, MOCK_VALUE_ONE, MOCK_VALUE_TWO);
  }

  @Test
  public void assert_GetItemModelList_POSETIVE() {
    List<ItemModel> itemList = new ArrayList<>();
    itemList.add(item);
    given(mockModelStore.getAll()).willReturn(Observable.just(itemList));

    itemDataRepository.getItemList();

    verify(mockStoreFactory).create();
    verify(mockModelStore).getAll();
  }

  @Test
  public void assert_GetItemModel_POSETIVE() {
    given(mockModelStore.get(MOCK_ID)).willReturn(Observable.just(item));
    itemDataRepository.getItem(MOCK_ID);

    verify(mockStoreFactory).create();
    verify(mockModelStore).get(MOCK_ID);
  }

  @Test
  public void assert_AddItemModel_POSETIVE(){
    given(mockModelStore.add(item)).willReturn(Observable.just(item));
    itemDataRepository.addItem(item);

    verify(mockStoreFactory).create();
    verify(mockModelStore).add(item);
  }

  @Test
  public void assert_DeleteItemModel_POSETIVE(){
    given(mockModelStore.delete(item)).willReturn(Observable.just(item));
    itemDataRepository.deleteItem(item);

    verify(mockStoreFactory).create();
    verify(mockModelStore).delete(item);
  }

  @Test
  public void assert_UpdateItemModel_POSETIVE(){
    given(mockModelStore.update(item)).willReturn(Observable.just(item));
    itemDataRepository.updateItem(item);

    verify(mockStoreFactory).create();
    verify(mockModelStore).update(item);
  }
}
