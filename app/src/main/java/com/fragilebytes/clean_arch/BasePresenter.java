package com.fragilebytes.clean_arch;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * class description: basic presenter
 * issues:
 */
public interface BasePresenter {
    void onPause();
    void onResume();
}
