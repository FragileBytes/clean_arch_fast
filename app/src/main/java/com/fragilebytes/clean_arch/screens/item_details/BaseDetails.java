package com.fragilebytes.clean_arch.screens.item_details;

import com.fragilebytes.clean_arch.BasePresenter;
import com.fragilebytes.domain.ItemModel;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 11/06/17, modified: 11/06/17
 * class description: this interface holds base Details View and Presenter Functions
 * issues:
 */

public interface BaseDetails {
    interface Presenter extends BasePresenter{
        void setView(BaseDetails.View view);
        void getItem(int id);
//        void setItemToView(ItemModel getItem);
        void updateItem(ItemModel item);
    }

    interface View{
        void setItemToView(ItemModel item);
        void notifyEntryUpdated(ItemModel item);
        void updateItem();
    }
}
