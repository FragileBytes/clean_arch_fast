package com.fragilebytes.clean_arch.screens.item_list;

import com.fragilebytes.clean_arch.BasePresenter;
import com.fragilebytes.domain.ItemModel;

import java.util.List;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * class description: this interface will hold base Functions of BaseItemView and BaseItemPresenter
 * issues:
 */
public interface BaseItemList {
    interface Presenter extends BasePresenter{
        void setView(BaseItemList.View view);
        void getItemList();
        void addItem();
        void deleteItem(ItemModel item);
        void showItemsInView(List<ItemModel> items);

    }

    interface View {
        void showItemsInView(List<ItemModel> items);
        void itemAdded(ItemModel item);
        void itemDeleted(ItemModel item);
        void showLoading();
        void hideLoading();

    }
}
