package com.fragilebytes.clean_arch.screens.item_details;

import android.util.Log;

import com.fragilebytes.domain.ItemModel;
import com.fragilebytes.domain.use_cases.GetItem;
import com.fragilebytes.domain.use_cases.UpdateItem;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * class description: this id DetailsPresenter implementation class
 * issues:
 */
public class DetailsPresenter implements BaseDetails.Presenter {

    private BaseDetails.View mView;
    private GetItem mGetItemUseCase;
    private UpdateItem mUpdateItemUseCase;

    @Inject
    public DetailsPresenter(GetItem getItemUseCase, UpdateItem updateItemUseCase) {
        mGetItemUseCase = getItemUseCase;
        mUpdateItemUseCase = updateItemUseCase;
    }

    @Override
    public void setView(BaseDetails.View view) {
        mView = view;
    }
    //get item by it id
    @Override
    public void getItem(int id) {
        mGetItemUseCase.execute(new DisposableObserver<ItemModel>() {
            @Override
            public void onNext(@NonNull ItemModel itemModel) {
                mView.setItemToView(itemModel);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.i("onError", e.getLocalizedMessage());

            }

            @Override
            public void onComplete() {

            }
        }, id);
    }

    //updates item
    @Override
    public void updateItem(ItemModel item) {
        if(item != null) {
            mUpdateItemUseCase.execute(new DisposableObserver<ItemModel>() {
                @Override
                public void onNext(@NonNull ItemModel item) {
                    mView.notifyEntryUpdated(item);
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    Log.i("onError", e.getLocalizedMessage());

                }

                @Override
                public void onComplete() {

                }
            }, item);
        }
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }
}
