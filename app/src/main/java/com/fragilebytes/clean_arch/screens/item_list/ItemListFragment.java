package com.fragilebytes.clean_arch.screens.item_list;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fragilebytes.clean_arch.R;
import com.fragilebytes.clean_arch.adapters.RecyclerViewAdapter;
import com.fragilebytes.clean_arch.di.components.ItemComponent;
import com.fragilebytes.clean_arch.interfaces.ActivityNotifier;
import com.fragilebytes.clean_arch.interfaces.FragmentNotifier;
import com.fragilebytes.clean_arch.screens.BaseFragment;
import com.fragilebytes.domain.ItemModel;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * class description: is is implementation of BaseItemList.View
 * issues:
 */
public class ItemListFragment extends BaseFragment implements BaseItemList.View, FragmentNotifier, RecyclerViewAdapter.RecyclerClickListener {

    @Inject
    ItemListPresenter presenter;

    @Bind(R.id.fragment_item_list_recycler)
    RecyclerView mRecyclerView;

    private RecyclerViewAdapter mRecyclerViewAdapter;

    public ActivityNotifier mActivityNotifier;

    private ProgressDialog mDialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.getComponent(ItemComponent.class).inject(this);
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        ButterKnife.bind(this, view);
        setUpProgressDialog();
        setUpRecycler();

        presenter.setView(this);
        return view;
    }

    //sets up progress dialog
    private void setUpProgressDialog(){
        mDialog = new ProgressDialog(getContext());
        mDialog.setTitle(getString(R.string.progress_dialog_title));
        mDialog.setMessage(getString(R.string.progress_dialog_message));
    }

    //sets up recycler view
    private void setUpRecycler(){
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerViewAdapter = new RecyclerViewAdapter(this);
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
    }

    //shows progress dialog
    @Override
    public void showLoading() {
        if(!mDialog.isShowing()) mDialog.show();

    }
    //hide progress dialog
    @Override
    public void hideLoading() {
        if(mDialog.isShowing()) mDialog.dismiss();
    }

    //shows updates items in recycler view
    @Override
    public void showItemsInView(List<ItemModel> items) {
        mRecyclerViewAdapter.setItems(items);
    }

    //adds item to recycler view
    @Override
    public void itemAdded(ItemModel item) {
        mRecyclerViewAdapter.addItem(item);
    }

    //delete item from recycler view
    @Override
    public void itemDeleted(ItemModel item) {
        mRecyclerViewAdapter.deleteItem(item);
    }

    //request for presenter to add an item
    @Override
    public void addItemInPresenter() {
        presenter.addItem();
    }

    @Override
    public void setItemInView(int id) {

    }

    //request presenter to update item
    @Override
    public void updateItemInView(ItemModel item) {
        presenter.getItemList();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((MainActivity)context).listFragNotifier = this;
        mActivityNotifier = ((MainActivity)context).mMainActivityNotifier;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    //handles on click performed on recycler view item
    @Override
    public void recyclerOnClick(ItemModel item) {
        mActivityNotifier.onItemSelected(item);
    }

    //handles remove event trigger by recycler view
    @Override
    public void recyclerOnRemove(ItemModel item) {
        mActivityNotifier.onItemRemoved();
        presenter.deleteItem(item);
    }
}
