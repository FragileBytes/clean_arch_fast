package com.fragilebytes.clean_arch.screens;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.fragilebytes.clean_arch.AndroidApplication;
import com.fragilebytes.clean_arch.di.components.ApplicationComponent;
import com.fragilebytes.clean_arch.di.modules.ActivityModule;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 11/06/17, modified: 11/06/17
 * class description: this is a base activity class that performs component injection, all shall extend this class
 * issues:
 */
public abstract class BaseActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    getApplicationComponent().inject(this);
  }

  /**
   * Get the Main Application component for dependency injection.
   *
   */
  protected ApplicationComponent getApplicationComponent() {
    return ((AndroidApplication) getApplication()).getApplicationComponent();
  }

  /**
   * Get an Activity module for dependency injection.
   *
   */
  protected ActivityModule getActivityModule() {
    return new ActivityModule(this);
  }
}
