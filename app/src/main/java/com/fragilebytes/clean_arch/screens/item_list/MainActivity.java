package com.fragilebytes.clean_arch.screens.item_list;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.fragilebytes.clean_arch.R;
import com.fragilebytes.clean_arch.di.HasComponent;
import com.fragilebytes.clean_arch.di.components.DaggerItemComponent;
import com.fragilebytes.clean_arch.di.components.ItemComponent;
import com.fragilebytes.clean_arch.interfaces.ActivityNotifier;
import com.fragilebytes.clean_arch.interfaces.FragmentNotifier;
import com.fragilebytes.clean_arch.interfaces.ItemRemovedCallback;
import com.fragilebytes.clean_arch.interfaces.ItemUpdatedCallback;
import com.fragilebytes.clean_arch.screens.BaseActivity;
import com.fragilebytes.clean_arch.screens.item_details.DetailsActivity;
import com.fragilebytes.clean_arch.screens.item_details.DetailsFragment;
import com.fragilebytes.domain.ItemModel;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * class description: this is implementation of main activity
 * issues:
 */

public class MainActivity extends BaseActivity implements HasComponent<ItemComponent>{

    public FragmentNotifier listFragNotifier;
    public ItemRemovedCallback itemRemovedCallback;


    ItemComponent itemComponent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeInjector();
        setContentView(R.layout.activity_main);
    }
    //callback that will be called by details view if item have been changed
    public ItemUpdatedCallback itemUpdated = new ItemUpdatedCallback() {
        @Override
        public void onItemUpdated(ItemModel item) {
            listFragNotifier.updateItemInView(item);
        }
    };

    public ActivityNotifier mMainActivityNotifier = new ActivityNotifier() {
        //when item selected
        @Override
        public void onItemSelected(ItemModel item) {
            DetailsFragment detailsFragment = (DetailsFragment) getSupportFragmentManager().findFragmentByTag("DetailsFragment");
            if(detailsFragment == null) {//start new activity if on a phone
                startActivity(DetailsActivity.getCallingIntent(getBaseContext(), item.getId()));
            }else{//update details fragment if on a tablet
                detailsFragment.setItemToView(item);
            }
        }
        //when item removed
        @Override
        public void onItemRemoved() {
            if(getSupportFragmentManager().findFragmentByTag("DetailsFragment") != null) {
                if (itemRemovedCallback != null) itemRemovedCallback.onItemRemoved();
            }
        }
    };

    private void initializeInjector() {
        this.itemComponent = DaggerItemComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .build();
    }

    @Override
    public ItemComponent getComponent() {
        return itemComponent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add: {//when add item menu item clicked
                if(listFragNotifier != null){
                    listFragNotifier.addItemInPresenter();
                }
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

}
