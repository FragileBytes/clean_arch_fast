package com.fragilebytes.clean_arch.screens.item_details;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.fragilebytes.clean_arch.R;
import com.fragilebytes.clean_arch.di.HasComponent;
import com.fragilebytes.clean_arch.di.components.DaggerItemComponent;
import com.fragilebytes.clean_arch.di.components.ItemComponent;
import com.fragilebytes.clean_arch.interfaces.FragmentNotifier;
import com.fragilebytes.clean_arch.screens.BaseActivity;
/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * This activity will hold the ItemDetailsFragment
 * */
public class DetailsActivity extends BaseActivity implements HasComponent<ItemComponent> {
    private static final String KEY_ID = "key_id";

    public static Intent getCallingIntent(Context context, int id) {
        Intent intent = new Intent(context, DetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_ID, id);
        intent.putExtras(bundle);
        return intent;
    }

    public FragmentNotifier detailsNotifier;

    private ItemComponent itemComponent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeInjector();
        setContentView(R.layout.activity_details);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            int id = bundle.getInt(KEY_ID);
            detailsNotifier.setItemInView(id);
        }
    }

    private void initializeInjector() {
        this.itemComponent = DaggerItemComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .build();
    }

    @Override
    public ItemComponent getComponent() {
        return itemComponent;
    }
}
