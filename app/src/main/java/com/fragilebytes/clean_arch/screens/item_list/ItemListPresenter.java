package com.fragilebytes.clean_arch.screens.item_list;

import android.util.Log;

import com.fragilebytes.domain.ItemModel;
import com.fragilebytes.domain.use_cases.AddItem;
import com.fragilebytes.domain.use_cases.DeleteItem;
import com.fragilebytes.domain.use_cases.GetItem;
import com.fragilebytes.domain.use_cases.GetItemList;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * class description: this is implementation class of item list presenter
 * issues:
 */
public class ItemListPresenter implements BaseItemList.Presenter {
    private BaseItemList.View mView;

    private final GetItemList mGetItemListUseCase;
    private final GetItem mGetItemUseCase;
    private final AddItem mAddItemUseCase;
    private final DeleteItem mDeleteItemUseCase;
    private CompositeDisposable mDisposable;

    @Inject
    public ItemListPresenter(GetItemList getItemListItemCase, GetItem getItemUseCase, AddItem addItemUseCase, DeleteItem deleteItemUseCase) {
        this.mGetItemListUseCase = getItemListItemCase;
        mGetItemUseCase = getItemUseCase;
        mAddItemUseCase = addItemUseCase;
        this.mDeleteItemUseCase = deleteItemUseCase;

        mDisposable = new CompositeDisposable();
    }

    @Override
    public void setView(BaseItemList.View view){
        mView = view;
//        mView.setPresenter(this);
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {
        getItemList();
    }

    //retrieves/updates item list
    public void getItemList() {
        mView.showLoading();
        mGetItemListUseCase.execute(new DisposableObserver<List<ItemModel>>() {
            @Override public void onComplete() {
                mView.hideLoading();
            }

            @Override public void onError(Throwable e) {
                Log.i("onError", e.getLocalizedMessage());
                mView.hideLoading();
            }

            @Override public void onNext(List<ItemModel> items) {
                showItemsInView(items);
            }
        }, null);
    }

    //adds item to list
    @Override
    public void addItem() {
        final ItemModel item = new ItemModel("value one", "value two");
        mAddItemUseCase.execute(new DisposableObserver<ItemModel>() {
            @Override
            public void onNext(@NonNull ItemModel itemModel) {
                mView.itemAdded(itemModel);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.i("onError", e.getLocalizedMessage());

            }

            @Override
            public void onComplete() {

            }
        }, item);
    }
    //deletes item
    @Override
    public void deleteItem(final ItemModel item) {
        mDeleteItemUseCase.execute(new DisposableObserver<ItemModel>() {
            @Override
            public void onNext(ItemModel item) {
                mView.itemDeleted(item);
            }

            @Override
            public void onError(Throwable e) {
                Log.i("onError", e.getLocalizedMessage());

            }

            @Override
            public void onComplete() {

            }
        }, item);
    }
    //requests view to show item list
    @Override
    public void showItemsInView(List<ItemModel> items) {
        mView.showItemsInView(items);
    }
}
