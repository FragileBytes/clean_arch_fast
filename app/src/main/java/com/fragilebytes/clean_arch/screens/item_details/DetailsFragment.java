package com.fragilebytes.clean_arch.screens.item_details;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.fragilebytes.clean_arch.R;
import com.fragilebytes.clean_arch.di.components.ItemComponent;
import com.fragilebytes.clean_arch.interfaces.FragmentNotifier;
import com.fragilebytes.clean_arch.interfaces.ItemRemovedCallback;
import com.fragilebytes.clean_arch.interfaces.ItemUpdatedCallback;
import com.fragilebytes.clean_arch.screens.BaseFragment;
import com.fragilebytes.clean_arch.screens.item_list.MainActivity;
import com.fragilebytes.domain.ItemModel;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * class description:
 * issues:
 */

public class DetailsFragment extends BaseFragment implements BaseDetails.View, FragmentNotifier{

    @Bind(R.id.fragment_details_value_one)
    EditText valueOneEdit;

    @Bind(R.id.fragment_details_value_two)
    EditText valueTwoEdit;

    @Inject
    DetailsPresenter mPresenter;

    private ItemModel currentItem;

    private ItemUpdatedCallback itemUpdated;

    public DetailsFragment(){setRetainInstance(true);}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getComponent(ItemComponent.class).inject(this);
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        ButterKnife.bind(this, view);
        mPresenter.setView(this);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if ( context instanceof DetailsActivity ) {
            DetailsActivity detailsActivity = ((DetailsActivity) context);
            detailsActivity.detailsNotifier = this;
        }
        else{
            itemUpdated =  ((MainActivity) context).itemUpdated;
            ((MainActivity) context).itemRemovedCallback = itemRemovedCallback;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
//        presenter.onDestroy();
    }

    @Override
    public void addItemInPresenter() {

    }

    @Override
    public void setItemInView(final int id) {
        mPresenter.getItem(id);
    }

    @Override
    public void updateItemInView(ItemModel item) {

    }
    //sends a request for view to update one of the items
    @Override
    public void setItemToView(ItemModel item) {
        currentItem = item;
        valueOneEdit.setText(item.getValueOne());
        valueTwoEdit.setText(item.getValueTwo());
    }
    //notify that entry is updated
    @Override
    public void notifyEntryUpdated(ItemModel item) {
        Toast.makeText(getContext(), getString(R.string.item_updated), Toast.LENGTH_LONG).show();
        if(itemUpdated != null) itemUpdated.onItemUpdated(item);
    }

    //updates item
    @OnClick(R.id.fragment_details_update_button)
    @Override
    public void updateItem() {
        if(currentItem != null) {
            currentItem.setValueOne(valueOneEdit.getText().toString());
            currentItem.setValueTwo(valueTwoEdit.getText().toString());

            mPresenter.updateItem(currentItem);
        }
    }
    //this callback can be triggered by main activity to notify details fragment that item has been removed
    private ItemRemovedCallback itemRemovedCallback = new ItemRemovedCallback() {
        @Override
        public void onItemRemoved() {
            valueOneEdit.setText("");
            valueTwoEdit.setText("");
            currentItem = null;
        }
    };
}
