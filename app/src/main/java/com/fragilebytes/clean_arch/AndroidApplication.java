package com.fragilebytes.clean_arch;

import android.app.Application;

import com.fragilebytes.clean_arch.di.components.ApplicationComponent;
import com.fragilebytes.clean_arch.di.components.DaggerApplicationComponent;
import com.fragilebytes.clean_arch.di.modules.ApplicationModule;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * class description: this application required to build dagger application components
 * issues:
 */
public class AndroidApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        initializeInjector();


    }


    private void initializeInjector() {
        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }

}
