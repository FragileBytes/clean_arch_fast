package com.fragilebytes.clean_arch.interfaces;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * class description:
 * issues:
 */
public interface ItemRemovedCallback {
    void onItemRemoved();
}
