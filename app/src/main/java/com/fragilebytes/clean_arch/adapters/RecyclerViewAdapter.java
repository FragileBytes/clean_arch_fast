package com.fragilebytes.clean_arch.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fragilebytes.clean_arch.R;
import com.fragilebytes.clean_arch.screens.item_list.ItemListFragment;
import com.fragilebytes.domain.ItemModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * class description: this is the basic adapter for recycler view
 * issues:
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    public interface RecyclerClickListener{
        void recyclerOnClick(ItemModel item);
        void recyclerOnRemove(ItemModel item);
    }

    private List<ItemModel> mItemModels;
    private Context mContext;
    private RecyclerClickListener recyclerClickListener;

    public RecyclerViewAdapter(ItemListFragment itemListFragment){
        mItemModels = new ArrayList<>();
        mContext = itemListFragment.getContext();
        recyclerClickListener = itemListFragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(mContext, R.layout.card_recycler_view_item , null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ItemModel itemModel = mItemModels.get(position);
        holder.mValueOneText.setText(itemModel.getValueOne());
        holder.mDeleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerClickListener.recyclerOnRemove(itemModel);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerClickListener.recyclerOnClick(itemModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemModels.size();
    }

    public void setItems(List<ItemModel> items){
        mItemModels.clear();
        mItemModels.addAll(items);
        notifyDataSetChanged();
    }

    public void addItem(ItemModel item){
        mItemModels.add(item);
        notifyItemInserted(mItemModels.size() - 1);
    }

    public void deleteItem(ItemModel item){
        int position = mItemModels.indexOf(item);
        mItemModels.remove(item);
        notifyItemRangeChanged(position, mItemModels.size());
        notifyItemRemoved(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView mValueOneText;
        public View mDeleteBtn;

    public ViewHolder(View itemView) {
        super(itemView);
        mValueOneText = (TextView) itemView.findViewById(R.id.card_recycler_view_item_title);
        mDeleteBtn = itemView.findViewById(R.id.card_recycler_view_delete_item);
    }
}
}
