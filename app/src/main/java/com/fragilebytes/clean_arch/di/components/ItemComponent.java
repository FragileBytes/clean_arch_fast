package com.fragilebytes.clean_arch.di.components;

import com.fragilebytes.clean_arch.di.PerActivity;
import com.fragilebytes.clean_arch.di.modules.ActivityModule;
import com.fragilebytes.clean_arch.di.modules.ItemModule;
import com.fragilebytes.clean_arch.screens.item_details.DetailsFragment;
import com.fragilebytes.clean_arch.screens.item_list.ItemListFragment;

import dagger.Component;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * Injects getItem specific Fragments.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, ItemModule.class})
public interface ItemComponent extends ActivityComponent {
  void inject(ItemListFragment itemListFragment);
  void inject(DetailsFragment detailsFragment);
}
