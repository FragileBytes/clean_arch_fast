package com.fragilebytes.clean_arch.di.modules;

import dagger.Module;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * Dagger module that provides getItem related collaborators.
 */
@Module
public class ItemModule {

  public ItemModule() {}
}
