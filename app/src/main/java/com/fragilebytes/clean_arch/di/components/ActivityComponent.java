package com.fragilebytes.clean_arch.di.components;

import android.app.Activity;
import android.content.Context;


import com.fragilebytes.clean_arch.di.PerActivity;
import com.fragilebytes.clean_arch.di.modules.ActivityModule;

import dagger.Component;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * base component upon which fragment's components may depend.
 * Activity-level components should extend this component.
 * Subtypes of ActivityComponent should be decorated with annotation:
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
interface ActivityComponent {

  //Exposed to sub-graphs.
  Activity activity();
  Context context();
}
