package com.fragilebytes.clean_arch.di.modules;

import android.content.Context;

import com.fragilebytes.clean_arch.AndroidApplication;
import com.fragilebytes.clean_arch.UIThread;
import com.fragilebytes.data.executor.JobExecutor;
import com.fragilebytes.data.repository.ItemDataRepository;
import com.fragilebytes.domain.executor.PostExecutionThread;
import com.fragilebytes.domain.executor.ThreadExecutor;
import com.fragilebytes.domain.repository.UserRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * Dagger module that provides objects which will live during the application lifecycle.
 */

@Module
public class ApplicationModule {
  private final AndroidApplication application;

  public ApplicationModule(AndroidApplication application) {
    this.application = application;
  }

  @Provides @Singleton Context provideApplicationContext() {
    return this.application;
  }

  @Provides @Singleton ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
    return jobExecutor;
  }

  @Provides @Singleton PostExecutionThread providePostExecutionThread(UIThread uiThread) {
    return uiThread;
  }

  @Provides @Singleton
  UserRepository provideItemRepository(ItemDataRepository itemDataRepository) {
    return itemDataRepository;
  }

}