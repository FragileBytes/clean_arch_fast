package com.fragilebytes.clean_arch.di.components;

import com.fragilebytes.clean_arch.di.modules.ApplicationModule;
import com.fragilebytes.clean_arch.screens.BaseActivity;
import com.fragilebytes.domain.executor.PostExecutionThread;
import com.fragilebytes.domain.executor.ThreadExecutor;
import com.fragilebytes.domain.repository.UserRepository;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * A component whose lifetime is the life of the application.
 */
@Singleton // Constraints this component to one-per-application or unscoped bindings.
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
  void inject(BaseActivity baseActivity);

  //Exposed to sub-graphs.
//  Context context();
  ThreadExecutor threadExecutor();
  PostExecutionThread postExecutionThread();
  UserRepository userRepository();
}
