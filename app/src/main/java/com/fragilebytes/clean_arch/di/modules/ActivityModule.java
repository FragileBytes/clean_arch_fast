package com.fragilebytes.clean_arch.di.modules;

import android.app.Activity;
import android.content.Context;

import com.fragilebytes.clean_arch.di.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * A module to wrap the Activity state and expose it to the graph.
 */
@Module
public class ActivityModule {
  private final Activity activity;

  public ActivityModule(Activity activity) {
    this.activity = activity;
  }

  /**
  * Expose the activity to dependents in the graph.
  */
  @Provides
  @PerActivity
  Activity activity() {
    return this.activity;
  }

  @Provides
  @PerActivity
  Context context(){
    return this.activity;
  }
}
