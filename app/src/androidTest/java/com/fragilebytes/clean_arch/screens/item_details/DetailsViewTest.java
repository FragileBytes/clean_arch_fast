package com.fragilebytes.clean_arch.screens.item_details;

import android.content.Context;
import android.support.test.rule.ActivityTestRule;
import android.support.v4.app.Fragment;

import com.fragilebytes.clean_arch.R;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * class description:
 * issues:
 */
public class DetailsViewTest {

    @Rule
    public ActivityTestRule<DetailsActivity> testRule = new ActivityTestRule<DetailsActivity>(DetailsActivity.class);
    private DetailsActivity mDetailsActivity;
    private Context mContext;

    private static final int FAKE_USER_ID = 3;

    @Before
    public void setUp() {
        mDetailsActivity = testRule.launchActivity(DetailsActivity.getCallingIntent(mContext, FAKE_USER_ID));
        mContext = mDetailsActivity.getBaseContext();
    }

    @Test
    public void assert_ItemsDetailsFragmentAttached() {
        Fragment detailFragment =
                mDetailsActivity.getSupportFragmentManager().findFragmentByTag(mContext.getString(R.string.tag_details_fragment));
        assertThat(detailFragment, is(notNullValue()));
    }

    @Test
    public void assert_ProperTitleSet(){
        String actualTitle = mDetailsActivity.getTitle().toString().trim();
        assertThat(actualTitle, is(mContext.getString(R.string.item_detail_activity_title)));
    }
//
//    public void testLoadUserHappyCaseViews() {
//        onView(withId(R.id.rl_retry)).check(matches(not(isDisplayed())));
//        onView(withId(R.id.rl_progress)).check(matches(not(isDisplayed())));
//
//        onView(withId(R.id.tv_fullname)).check(matches(isDisplayed()));
//        onView(withId(R.id.tv_email)).check(matches(isDisplayed()));
//        onView(withId(R.id.tv_description)).check(matches(isDisplayed()));
//    }
//
//    public void testLoadUserHappyCaseData() {
//        onView(withId(R.id.tv_fullname)).check(matches(withText("John Sanchez")));
//        onView(withId(R.id.tv_email)).check(matches(withText("dmedina@katz.edu")));
//        onView(withId(R.id.tv_followers)).check(matches(withText("4523")));
//    }
}