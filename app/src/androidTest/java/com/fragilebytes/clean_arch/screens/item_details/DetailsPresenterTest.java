package com.fragilebytes.clean_arch.screens.item_details;

import android.content.Context;
import android.telecom.Call;

import com.fragilebytes.domain.ItemModel;
import com.fragilebytes.domain.use_cases.GetItem;
import com.fragilebytes.domain.use_cases.UpdateItem;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.observers.DisposableObserver;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * class description:
 * issues:
 */
@RunWith(MockitoJUnitRunner.class)
public class DetailsPresenterTest {

    private static final int MOCK_ID = 1;

    private DetailsPresenter mDetailsPresenter;

    @Mock
    private BaseDetails.View mockView;
    @Mock
    private GetItem mockGetItemUseCase;
    @Mock
    private UpdateItem mockUpdateItemUseCase;
    @Mock
    private ItemModel mockItem;

    @Before
    public void setUp() {
        mDetailsPresenter = new DetailsPresenter(mockGetItemUseCase, mockUpdateItemUseCase);
        mDetailsPresenter.setView(mockView);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void assert_GetItem() {
        mDetailsPresenter.getItem(MOCK_ID);
        verify(mockGetItemUseCase).execute(any(DisposableObserver.class), any(Integer.class));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void assert_UpdateItem() {
        mDetailsPresenter.updateItem(mockItem);
        verify(mockUpdateItemUseCase).execute(any(DisposableObserver.class), any(ItemModel.class));
    }
}
