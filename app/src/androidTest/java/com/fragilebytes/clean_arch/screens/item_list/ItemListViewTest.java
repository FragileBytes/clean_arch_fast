package com.fragilebytes.clean_arch.screens.item_list;

import android.content.Context;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.v4.app.Fragment;

import com.fragilebytes.clean_arch.R;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.AdditionalMatchers.not;

/**
 * Aleksandr Kotikov, fragile_bytes
 * fragilebytes@gmail.com
 * created: 12/06/17, modified: 12/06/17
 * class description:
 * issues:
 */
public class ItemListViewTest {
    @Rule
    public ActivityTestRule<MainActivity> testRule = new ActivityTestRule<MainActivity>(MainActivity.class);
    private MainActivity mMainActivity;
    private Context mContext;

    private String TEST_VALUE_ONE = "TESTING VALUE ONE";


    @Before
    public void setUp() throws Exception{
//        this.setActivityIntent(createTargetIntent());
        mMainActivity = testRule.getActivity();
        mContext = mMainActivity.getBaseContext();
    }

    @Test
    public void assert_ItemListFragmentAttached() {
        Fragment userListFragment =
                mMainActivity.getSupportFragmentManager().findFragmentByTag(mContext.getString(R.string.tag_item_list_fragment));
        assertThat(userListFragment, is(notNullValue()));
    }

    @Test
    public void assert_ProperTitleSet(){
        String actualTitle = mMainActivity.getTitle().toString().trim();

        assertThat(actualTitle, is(mContext.getString(R.string.item_list_activity_title)));
    }

    @Test
    public void assert_AddNewItemEditAndBack(){

        onView(withId(R.id.action_add)).perform(click());
        onView(withId(R.id.fragment_item_list_recycler))
                .perform(RecyclerViewActions
                        .actionOnItemAtPosition(0, click()));
        onView(withId(R.id.fragment_details_value_one)).perform(clearText(), typeText(TEST_VALUE_ONE));
        onView(withText(mContext.getString(R.string.update))).perform(click());
        mMainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMainActivity.onBackPressed();
            }
        });

//        try {
//            onView(withText(TEST_VALUE_ONE)).check(matches(isDisplayed()));
//        } catch (NoMatchingViewException e) {
//            onView(withId(R.id.action_add)).perform(click());
//            onView(withId(R.id.fragment_item_list_recycler))
//                    .perform(RecyclerViewActions
//                            .actionOnItemAtPosition(0, click()));
//            onView(withId(R.id.fragment_details_value_one)).perform(clearText(), typeText(TEST_VALUE_ONE));
//            onView(withText(mContext.getString(R.string.update))).perform(click());
//            mMainActivity.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    mMainActivity.onBackPressed();
//                }
//            });
//
//        }
    }
}